package pmu.bor.landappkp.models;

/**
 * Created by Rashid Ahmad on 1/31/2018.
 */

public class Moza {
    private String id;
    private String name;
    private boolean isLive;

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return  this.id;
    }

    public String getName() {
        return this.name;
    }

    public void setIsLive(boolean isLive) {
        this.isLive = isLive;
    }

    public boolean getIsLive() {
        return  this.isLive;
    }
}
