package pmu.bor.landappkp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import me.anwarshahriar.calligrapher.Calligrapher;
import pmu.bor.landappkp.helpers.Settings;

public class DashboardActivity extends AppCompatActivity {


    private LinearLayout btn_fard_name_khata;
    private LinearLayout btn_fard_cnic;
    private LinearLayout btn_intiqal_token;
    private LinearLayout btn_intiqal_number;
    private LinearLayout btn_intiqal_nic;
    private LinearLayout btnDisclaimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        Settings settings = new Settings();
        Calligrapher calligrapher = new Calligrapher(this);
        calligrapher.setFont(this, settings.getUrduFontPath(), true);

        btn_fard_name_khata = (LinearLayout) findViewById(R.id.btn_fard_name_khata);
        btn_fard_name_khata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(v.getContext(), InputSelectionActivity.class);
                startActivity(intent);

            }
        });
        btn_fard_cnic = (LinearLayout) findViewById(R.id.btn_fard_cnic);
        btn_fard_cnic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), FardbyNICActivity.class);
                startActivity(intent);
            }
        });
        btn_intiqal_token = (LinearLayout) findViewById(R.id.btn_intiqal_token);
        btn_intiqal_token.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(v.getContext(), MutationByTokenActivity.class);
                startActivity(intent);

            }
        });
        btn_intiqal_number = (LinearLayout) findViewById(R.id.btn_intiqal_number);
        btn_intiqal_number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), MutationByNoActivity.class);
                startActivity(intent);
            }
        });

        btn_intiqal_nic = (LinearLayout) findViewById(R.id.btn_intiqal_nic);
        btn_intiqal_nic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), MutationByNicActivity.class);
                startActivity(intent);
            }
        });

        btnDisclaimer = (LinearLayout) findViewById(R.id.btn_disclaimer);
        btnDisclaimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), DisclaimerActivity.class);
                startActivity(intent);
            }
        });
    }
}
