package pmu.bor.landappkp.models;

/**
 * Created by Rashid Ahmad on 2/6/2018.
 */

public class Khata {
    private String id;
    private String khata;

    public void setId(String id) {
        this.id = id;
    }

    public void setKhata(String khata) {
        this.khata = khata;
    }

    public String getId() {
        return this.id;
    }

    public String getKhata() {
        return this.khata;
    }
}
