package pmu.bor.landappkp;

import android.app.ProgressDialog;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import me.anwarshahriar.calligrapher.Calligrapher;
import pmu.bor.landappkp.helpers.Settings;
import pmu.bor.landappkp.helpers.URLStore;
import pmu.bor.landappkp.models.Result;

public class MutationResultActivity extends AppCompatActivity {

    ProgressDialog progressDialog;
    TextView txtNoRecordMessage;
    TextView txtResult;
    LinearLayout layoutNoRecord;
    URLStore urls;
    String intiqalNo = "";
    String tehsilId = "";
    String mozaId = "";
    String tokenNo = "";
    String nic = "";
    String intiqalDate = "";
    String searchResult = "";
    String searchBy = "";
    Typeface appFontFace;
    ArrayAdapter<Result> resultArrayAdapter;
    RequestQueue requestQueue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mutation_result);

        Settings settings = new Settings();
        Calligrapher calligrapher = new Calligrapher(this);
        calligrapher.setFont(this, settings.getUrduFontPath(), true);
        appFontFace = Typeface.createFromAsset(getAssets(), settings.getUrduFontPath());

        requestQueue = Volley.newRequestQueue(this);
        txtResult = (TextView) findViewById(R.id.lbl_result);
        txtNoRecordMessage = (TextView) findViewById(R.id.lbl_norecord);
        layoutNoRecord = (LinearLayout) findViewById(R.id.layout_norecord);
        //txtNoRecordMessage.setVisibility(View.GONE);
        layoutNoRecord.setVisibility(View.GONE);

        urls = new URLStore();
        searchBy = getIntent().getStringExtra("searchBy");

        if (searchBy.equals("token")) {
            intiqalDate = getIntent().getStringExtra("intiqalDate");
            tokenNo = getIntent().getStringExtra("tokenNo");
            tehsilId = getIntent().getStringExtra("tehsilId");
        } else if (searchBy.equals("nic")) {
            nic = getIntent().getStringExtra("nic");
        }else {
            intiqalNo = getIntent().getStringExtra("intiqalNo");
            mozaId = getIntent().getStringExtra("mozaId");
        }
        loadResult();
    }

    private void loadResult() {
        String url = "";
        if (searchBy.equals("token")) {
            String[] dateParts = intiqalDate.split("/");
            String sqlFormat = dateParts[2] + "-" + dateParts[1] + "-" + dateParts[0];
            url = urls.GET_INTIQAL_STATUS_BY_TOKEN + "token=" + tokenNo + "&dated=" + sqlFormat + "&tehsilId=" + tehsilId;
        } else {
            url = urls.GET_INTIQAL_STATUS_BY_NO + "intiqalNo=" + intiqalNo + "&mozaId=" + mozaId;
        }

        progressDialog = new ProgressDialog(MutationResultActivity.this);
        progressDialog.setMessage("Please Wait");
        progressDialog.setCancelable(false);
        progressDialog.show();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("Message").equals("Not Found")) {
                                txtResult.setVisibility(View.GONE);
                                //txtNoRecordMessage.setVisibility(View.VISIBLE);
                                layoutNoRecord.setVisibility(View.VISIBLE);
                            } else {
                                String resultStr = response.getString("Message");
                                String[] resultArr = resultStr.split(",");
                                String mutationStatusStr = "";

                                for (int i=0; i < resultArr.length; i++) {
                                    mutationStatusStr += resultArr[i] + "\n";
                                }
                                txtResult.setTypeface(txtResult.getTypeface(), Typeface.BOLD);
                                txtResult.setText(mutationStatusStr);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        progressDialog.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();

                    }
                });
        //add request to queue
        requestQueue.add(jsonObjectRequest);
    }
}
