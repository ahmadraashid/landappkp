package pmu.bor.landappkp;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SplashActivity extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 65;
    private static int myProgress = 0;
    private int progressStatus = 0;
    private Handler myHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        /*Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();*/

        beginYourTask(50);

    }

    public void beginYourTask(final int timeout) {
        myProgress = 0;
        //progressBar.setMax(100);

        new Thread(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                while (progressStatus < 100) {
                    progressStatus = performTask();
                    myHandler.post(new Runnable() {
                        public void run() {

                            //progressWheelSplash.setProgress(progressStatus);

                        }
                    });

                }
                myHandler.post(new Runnable() {

                    @Override
                    public void run() {
                        // TODO Auto-generated method stub


                        Intent dashboardActivity;

                        dashboardActivity = new Intent(SplashActivity.this, DashboardActivity.class);
                        startActivity(dashboardActivity);


                        finish();


                    }
                });

            }

            private int performTask() {
                try {
                    // ---Do some task---
                    Thread.sleep(timeout);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return ++myProgress;
            }
        }).start();
    }
}
