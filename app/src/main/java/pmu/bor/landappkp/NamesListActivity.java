package pmu.bor.landappkp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.anwarshahriar.calligrapher.Calligrapher;
import pmu.bor.landappkp.helpers.Settings;
import pmu.bor.landappkp.helpers.URLStore;
import pmu.bor.landappkp.models.Result;
import pmu.bor.landappkp.models.UserNames;

public class NamesListActivity extends AppCompatActivity {

    ProgressDialog progressDialog;
    ListView listView;
    TextView txtNoRecordMessage;
    URLStore urls;
    String selectedMozaId = "", selectedName = "", selectedFatherName = "";
    ArrayList<UserNames> usersList;
    ArrayList<String> results;
    ArrayList<String> noRecordMessage;
    ArrayAdapter<String> noRecordAdapter;
    ArrayAdapter<UserNames> resultArrayAdapter;
    RequestQueue requestQueue;
    Typeface appFontFace;
    LinearLayout norecordLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_names_list);

        Settings settings = new Settings();
        Calligrapher calligrapher = new Calligrapher(this);
        calligrapher.setFont(this, settings.getUrduFontPath(), true);
        appFontFace = Typeface.createFromAsset(getAssets(), settings.getUrduFontPath());

        requestQueue = Volley.newRequestQueue(this);
        listView = (ListView) findViewById(R.id.lv_result);
        norecordLayout = (LinearLayout) findViewById(R.id.layout_norecord);
        norecordLayout.setVisibility(View.GONE);
        //txtNoRecordMessage = (TextView) findViewById(R.id.lbl_norecord);
        //txtNoRecordMessage.setVisibility(View.GONE);

        usersList = new ArrayList<UserNames>();
        urls = new URLStore();
        selectedMozaId = getIntent().getStringExtra("selectedMozaId").toString();
        selectedName = getIntent().getStringExtra("selectedName").toString();
        selectedFatherName = getIntent().getStringExtra("selectedFatherName").toString();
        //Load list of names for the provided parameters
        loadResult();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String selectedPersonId = usersList.get(i).getPersonId();
                Intent intent = new Intent(view.getContext(), UserMalkiatActivity.class);
                intent.putExtra("searchCriteria", selectedPersonId);
                intent.putExtra("searchType", "1");
                startActivity(intent);
            }
        });
    }


    class ResultAdapter extends ArrayAdapter<UserNames> {
        private Context context;
        List<UserNames> dataResults;

        public ResultAdapter(Context context, int resource, List<UserNames> objects) {
            super(context, resource, objects);

            this.context = context;
            this.dataResults = objects;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            UserNames resultObj = usersList.get(position);
            String name = resultObj.getName();
            //get the inflater and inflate the XML layout for each item
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            //conditionally inflate either standard or special template
            View view;
            view = inflater.inflate(R.layout.list_item_names, null);

            CheckedTextView txtName = (CheckedTextView) view.findViewById(R.id.txtName);
            txtName.setCheckMarkDrawable(android.R.drawable.btn_radio);
            txtName.setText(name);
            txtName.setTypeface(appFontFace);
            return view;
        }
    }

    private void loadResult() {
        String url = urls.MATCHING_NAMES_LIST_MOZA;
        progressDialog = new ProgressDialog(NamesListActivity.this);
        progressDialog.setMessage("Please Wait");
        progressDialog.setCancelable(false);
        progressDialog.show();

        JSONObject postData = new JSONObject();
        try {
            postData.put("FullName",selectedName);
            postData.put("FatherName", selectedFatherName);
            postData.put("MozaId", selectedMozaId);
        }catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST,url, postData,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.d("Response", response.toString());
                            JSONArray dataArray = response.getJSONArray("Result");

                            if (dataArray.length() == 0) {
                                listView.setVisibility(View.GONE);
                                norecordLayout.setVisibility(View.VISIBLE);
                                //txtNoRecordMessage.setVisibility(View.VISIBLE);
                            } else {
                                for (int i = 0; i < dataArray.length(); i++) {
                                    try {
                                        String data = "";
                                        JSONObject obj = dataArray.getJSONObject(i);

                                        UserNames resultObj = new UserNames();
                                        resultObj.setName(obj.getString("FullName"));
                                        resultObj.setPersonId(obj.getString("PersonId"));
                                        usersList.add(resultObj);
                                    } catch (JSONException e) {
                                        Log.d("Error: ", e.getMessage());
                                    }
                                }
                                resultArrayAdapter = new ResultAdapter(NamesListActivity.this, 0, usersList);
                                listView.setAdapter(resultArrayAdapter);
                            }
                            progressDialog.dismiss();
                        } catch(JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                        VolleyLog.d("Error", "Error: " + error.getMessage());
                    }
                }) {

            /**
             * Passing some request headers
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };
        requestQueue.add(request);
        //requestQueue.add(postRequest);
        /*JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        results = new ArrayList<String>();
                        try {
                            JSONArray jsonArray = response.getJSONArray("Result");
                            if (jsonArray.length() == 0) {
                                listView.setVisibility(View.GONE);
                                txtNoRecordMessage.setVisibility(View.VISIBLE);
                            } else {
                                JSONObject oneObject = jsonArray.getJSONObject(0);

                                // display the data in UI
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    try {
                                        String data = "";
                                        JSONObject obj = jsonArray.getJSONObject(i);

                                        UserNames resultObj = new UserNames();
                                        resultObj.setName(obj.getString("PersonName"));
                                        usersList.add(resultObj);
                                        //adapter.add(data);
                                    } catch (JSONException e) {
                                        Log.d("Error: ", e.getMessage());
                                    }
                                }
                                resultArrayAdapter = new NamesListActivity.ResultAdapter(NamesListActivity.this, 0, usersList);
                                listView.setAdapter(resultArrayAdapter);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        progressDialog.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        //add request to queue
        requestQueue.add(jsonObjectRequest);*/
    }

}
