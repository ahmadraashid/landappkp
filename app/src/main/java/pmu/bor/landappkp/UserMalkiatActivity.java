package pmu.bor.landappkp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import me.anwarshahriar.calligrapher.Calligrapher;
import pmu.bor.landappkp.helpers.Settings;
import pmu.bor.landappkp.helpers.URLStore;
import pmu.bor.landappkp.models.PersonMalkiat;

public class UserMalkiatActivity extends AppCompatActivity {

    ProgressDialog progressDialog;
    ListView listView;
    TextView txtNoRecordMessage;
    TextView txtOwner;
    URLStore urls;
    String searchType = "";
    String searchCriteria = "";
    ArrayList<PersonMalkiat> malkiatResults;
    ArrayList<String> results;
    ArrayList<String> noRecordMessage;
    ArrayAdapter<String> noRecordAdapter;
    ArrayAdapter<PersonMalkiat> resultArrayAdapter;
    RequestQueue requestQueue;
    Typeface appFontFace;
    LinearLayout layout_norecord;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_malkiat);

        Settings settings = new Settings();
        Calligrapher calligrapher = new Calligrapher(this);
        calligrapher.setFont(this, settings.getUrduFontPath(), true);
        appFontFace = Typeface.createFromAsset(getAssets(), settings.getUrduFontPath());

        requestQueue = Volley.newRequestQueue(this);
        listView = (ListView) findViewById(R.id.lv_result);
        txtNoRecordMessage = (TextView) findViewById(R.id.lbl_norecord);
        layout_norecord = (LinearLayout) findViewById(R.id.layout_norecord);
        //txtNoRecordMessage.setVisibility(View.GONE);
        txtOwner = (TextView) findViewById(R.id.txt_maalik);
        txtOwner.setVisibility(View.GONE);
        layout_norecord.setVisibility(View.GONE);

        malkiatResults = new ArrayList<PersonMalkiat>();
        urls = new URLStore();

        searchType = getIntent().getStringExtra("searchType");
        searchCriteria = getIntent().getStringExtra("searchCriteria");

        loadResult();
    }

    private void loadResult() {
        String url = "";
        if (searchType.equals("1")) {
            url = urls.GET_USER_MALKIAT_BYPERSONID + searchCriteria;
        } else {
            url = urls.GET_USER_MALKIAT_BYNIC + searchCriteria;
        }

        progressDialog = new ProgressDialog(UserMalkiatActivity.this);
        progressDialog.setMessage("Please Wait");
        progressDialog.setCancelable(false);
        progressDialog.show();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        results = new ArrayList<String>();
                        try {
                            JSONArray jsonArray = response.getJSONArray("Result");
                            //Log.d("Response is: ", response.toString());
                            if (jsonArray.length() == 0) {
                                listView.setVisibility(View.GONE);
                                //txtNoRecordMessage.setVisibility(View.VISIBLE);
                                layout_norecord.setVisibility(View.VISIBLE);
                            } else {
                                txtOwner.setVisibility(View.VISIBLE);
                                JSONObject oneObject = jsonArray.getJSONObject(0);
                                // display the data in UI
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    try {
                                        String data = "";
                                        JSONObject obj = jsonArray.getJSONObject(i);
                                        PersonMalkiat resultObj = new PersonMalkiat();
                                        resultObj.setTehsil(obj.getString("Tehsil"));
                                        resultObj.setMoza(obj.getString("Moza"));
                                        resultObj.setKhataNo(obj.getString("KhataNo"));
                                        resultObj.setPersonName(obj.getString("PersonName"));
                                        resultObj.setFardAreaPart(obj.getString("FardAreaPart"));
                                        resultObj.setFardArea(obj.getString("FardArea"));
                                        resultObj.setMalkiatType(obj.getString("MalkiatType"));
                                        malkiatResults.add(resultObj);
                                        if (i == 0) {
                                            txtOwner.setText("نام مالک: " + resultObj.getPersonName());
                                        }
                                        //adapter.add(data);
                                    } catch (JSONException e) {
                                        Log.d("Error: ", e.getMessage());
                                    }
                                }
                                resultArrayAdapter = new UserMalkiatActivity.ResultAdapter(UserMalkiatActivity.this, 0, malkiatResults);
                                listView.setAdapter(resultArrayAdapter);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        progressDialog.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(),error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
        //add request to queue
        requestQueue.add(jsonObjectRequest);
    }

    class ResultAdapter extends ArrayAdapter<PersonMalkiat> {
        private Context context;
        List<PersonMalkiat> dataResults;

        public ResultAdapter(Context context, int resource, List<PersonMalkiat> objects) {
            super(context, resource, objects);

            this.context = context;
            this.dataResults = objects;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            PersonMalkiat resultObj = malkiatResults.get(position);
            String tehsil = resultObj.getTehsil();
            String moza = resultObj.getMoza();
            String khataNo = resultObj.getKhataNo();
            String fardAreaPart = resultObj.getFardAreaPart();
            String fardArea = resultObj.getFardArea();
            String malkiatType = resultObj.getMalkiatType();
            String arr[] = fardArea.split("-");
            String totalPartStr = "";

            if (arr.length > 0) {
                for(int i=0; i < arr.length; i++) {
                    switch (i) {
                        case 0:
                            totalPartStr = arr[i] + " کنال ";
                            break;

                        case 1:
                            totalPartStr += arr[i] + " مرلہ ";
                            break;

                        case 2:
                            totalPartStr += arr[i] + " فٹ ";
                            break;
                    }
                }
            }

            //get the inflater and inflate the XML layout for each item
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            //conditionally inflate either standard or special template
            View view;
            view = inflater.inflate(R.layout.list_item_malkiat, null);

            TextView txtKhataLbl = (TextView) view.findViewById(R.id.txtKhataLabel);
            TextView txtPartLbl = (TextView) view.findViewById(R.id.txtPartLabel);
            TextView txtMalkiatTypeLbl = (TextView) view.findViewById(R.id.txtMalkiatTypeLabel);
            TextView txtTotalPartLbl = (TextView) view.findViewById(R.id.txtTotalPartLabel);
            TextView txtTehsilLbl = (TextView) view.findViewById(R.id.txtTehsilLabel);
            TextView txtMozaLbl = (TextView) view.findViewById(R.id.txtMozaLabel);
            txtTehsilLbl.setTypeface(appFontFace, Typeface.BOLD);
            txtMozaLbl.setTypeface(appFontFace, Typeface.BOLD);
            txtKhataLbl.setTypeface(appFontFace, Typeface.BOLD);
            txtPartLbl.setTypeface(appFontFace, Typeface.BOLD);
            txtTotalPartLbl.setTypeface(appFontFace, Typeface.BOLD);
            txtMalkiatTypeLbl.setTypeface(appFontFace, Typeface.BOLD);

            TextView txtTehsil = (TextView) view.findViewById(R.id.txtTehsil);
            TextView txtMoza = (TextView) view.findViewById(R.id.txtMoza);
            TextView txtKhata = (TextView) view.findViewById(R.id.txtKhata);
            TextView txtPart = (TextView) view.findViewById(R.id.txtPart);
            TextView txtMalkiatType = (TextView) view.findViewById(R.id.txtMalkiatType);
            TextView txtTotalPart = (TextView) view.findViewById(R.id.txtTotalPart);
            txtTehsil.setTypeface(appFontFace);
            txtMoza.setTypeface(appFontFace);
            txtKhata.setTypeface(appFontFace);
            txtPart.setTypeface(appFontFace);
            txtMalkiatType.setTypeface(appFontFace);
            txtTotalPart.setTypeface(appFontFace);

            txtTehsil.setText(tehsil);
            txtMoza.setText(moza);
            txtKhata.setText(khataNo);
            txtPart.setText(fardAreaPart);
            txtMalkiatType.setText(malkiatType);
            txtTotalPart.setText(totalPartStr);

            return view;
        }
    }
}
