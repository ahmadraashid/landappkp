package pmu.bor.landappkp.models;

/**
 * Created by Rashid Ahmad on 3/2/2018.
 */

public class PersonMalkiat {
    private String tehsil;
    private String moza;
    private String khataNo;
    private String personName;
    private String malkiatType;
    private String fardArea;
    private String fardAreaPart;

    public void setTehsil(String tehsil) {
        this.tehsil = tehsil;
    }

    public String getTehsil() {
        return  this.tehsil;
    }

    public void setMoza(String moza) {
        this.moza = moza;
    }

    public String getMoza() {
        return this.moza;
    }

    public void setKhataNo(String khata) {
        this.khataNo = khata;
    }

    public String getKhataNo() {
        return this.khataNo;
    }

    public void setPersonName(String name) {
        this.personName = name;
    }

    public String getPersonName() {
        return this.personName;
    }

    public void setMalkiatType(String type) {
        this.malkiatType = type;
    }

    public String getMalkiatType() {
        return this.malkiatType;
    }

    public void setFardArea(String area) {
        this.fardArea = area;
    }

    public String getFardArea() {
        return this.fardArea;
    }

    public void setFardAreaPart(String area) {
        this.fardAreaPart = area;
    }

    public String getFardAreaPart() {
        return this.fardAreaPart;
    }
}
