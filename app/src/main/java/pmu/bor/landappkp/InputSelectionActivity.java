package pmu.bor.landappkp;

import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.renderscript.ScriptGroup;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import me.anwarshahriar.calligrapher.Calligrapher;
import pmu.bor.landappkp.helpers.EngToUrduCharMap;
import pmu.bor.landappkp.helpers.Settings;
import pmu.bor.landappkp.helpers.URLStore;
import pmu.bor.landappkp.models.District;
import pmu.bor.landappkp.models.Khata;
import pmu.bor.landappkp.models.Moza;
import pmu.bor.landappkp.models.Tehsil;

public class InputSelectionActivity extends AppCompatActivity {

    private Spinner districtSpinner;
    private Spinner tehsilSpinner;
    private Spinner mozaSpinner;
    private Spinner searchBySpinner;
    private Spinner khataSpinner;

    Button btnFind;
    JSONObject jsonobject;
    JSONArray jsonarray;
    ArrayList<District> districtsList;
    ArrayList<Tehsil> tehsilsList;
    ArrayList<Moza> mozasList;
    ArrayList<Khata> khatasList;
    ProgressDialog progressDialog;
    CustomSpinnerAdapter customDistrictSpinnerAdapter;
    CustomSpinnerAdapter customTehsilSpinnerAdapter;
    CustomSpinnerMozaAdapter customMozaSpinnerAdapter;
    CustomSpinnerAdapter customKhataSpinnerAdapter;
    CustomSpinnerAdapter customSearchSpinnerAdapter;
    ArrayAdapter<String> adapter;
    ArrayAdapter<String> districtAdapter;
    ArrayAdapter<String> tehsilAdapter;
    ArrayAdapter<String> mozaAdapter;
    ArrayAdapter<String> khataAdapter;
    String selectedDistrictId = "";
    String selectedTehsilId = "";
    String selectedMozaId = "";
    String searchBy = "";
    String selectedKhataId = "";
    Typeface appTextTypeFace;
    //String selectedName = "";
    //String selectedFatherName = "";
    URLStore urls;
    RequestQueue requestQueue;
    ArrayList<String> emptyList;
    int selectedSearchByIndex = 0;
    ArrayList<Moza> emptyMozasList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_selection);

        Settings settings = new Settings();
        Calligrapher calligrapher = new Calligrapher(this);
        calligrapher.setFont(this, settings.getUrduFontPath(), true);
        appTextTypeFace = Typeface.createFromAsset(getAssets(), settings.getUrduFontPath());

        emptyList = new ArrayList<String>();
        emptyMozasList = new ArrayList<Moza>();
        requestQueue = Volley.newRequestQueue(this);
        districtSpinner = (Spinner) findViewById(R.id.district_spinner);
        tehsilSpinner = (Spinner) findViewById(R.id.tehsil_spinner);
        mozaSpinner = (Spinner) findViewById(R.id.moza_spinner);
        searchBySpinner = (Spinner) findViewById(R.id.searchby_spinner);
        khataSpinner = (Spinner) findViewById(R.id.khatas_spinner);

        btnFind = (Button) findViewById(R.id.btn_find);

        khataSpinner.setVisibility(View.GONE);

        urls = new URLStore();
        districtsList = new ArrayList<District>();
        tehsilsList = new ArrayList<Tehsil>();
        mozasList = new ArrayList<Moza>();
        khatasList = new ArrayList<Khata>();
        progressDialog = new ProgressDialog(InputSelectionActivity.this);
        progressDialog.setMessage("Please Wait");
        progressDialog.setCancelable(false);
        final EngToUrduCharMap charMapper = new EngToUrduCharMap();
        loadDistrictData();
        SearchByAsyncTasks sTasks = new SearchByAsyncTasks();
        sTasks.execute();

        btnFind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                AlertDialog.Builder builder = new AlertDialog.Builder(InputSelectionActivity.this);
                // Add the buttons
                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User clicked OK button
                    }
                });

                switch(selectedSearchByIndex) {
                    case 2:
                        if (selectedKhataId.length() == 0 || selectedKhataId.equals("0")) {
                            builder.setMessage(R.string.khata_required);
                            AlertDialog dialog = builder.create();
                            dialog.show();
                        } else {
                            intent = new Intent(v.getContext(), ResultActivity.class);
                            intent.putExtra("khataId", selectedKhataId);
                            startActivityForResult(intent, 0);
                        }
                        break;

                    default:
                        builder.setMessage(R.string.invalid_request);
                        AlertDialog dialog = builder.create();
                        dialog.show();
                        break;
                }

            }
        });

        //Drop down selections handling

        districtSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    selectedDistrictId = districtsList.get(position).getId();
                    loadTehsilData();
                } else {
                    selectedDistrictId = "0";
                    customTehsilSpinnerAdapter = new CustomSpinnerAdapter(InputSelectionActivity.this, emptyList);
                    tehsilSpinner.setAdapter(customTehsilSpinnerAdapter);
                }

                customMozaSpinnerAdapter = new CustomSpinnerMozaAdapter(InputSelectionActivity.this, emptyMozasList);
                customKhataSpinnerAdapter = new CustomSpinnerAdapter(InputSelectionActivity.this, emptyList);
                mozaSpinner.setAdapter(customMozaSpinnerAdapter);
                khataSpinner.setAdapter(customKhataSpinnerAdapter);
                searchBySpinner.setSelection(0);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        //Tehsils Spinner
        tehsilSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    selectedTehsilId = tehsilsList.get(position).getId();

                    loadMozaData();
                } else {
                    selectedTehsilId = "0";
                    selectedMozaId = "0";

                    customMozaSpinnerAdapter = new CustomSpinnerMozaAdapter(InputSelectionActivity.this, emptyMozasList);
                    mozaSpinner.setAdapter(customMozaSpinnerAdapter);
                }
                customKhataSpinnerAdapter = new CustomSpinnerAdapter(InputSelectionActivity.this, emptyList);
                khataSpinner.setAdapter(customKhataSpinnerAdapter);
                searchBySpinner.setSelection(0);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        //Moza Spinner
        mozaSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    selectedMozaId = mozasList.get(position).getId();
                } else {
                    selectedMozaId = "0";
                    selectedKhataId = "0";
                    customKhataSpinnerAdapter = new CustomSpinnerAdapter(InputSelectionActivity.this, emptyList);
                    khataSpinner.setAdapter(customKhataSpinnerAdapter);
                }
                searchBySpinner.setSelection(0);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Khata Spinner
        khataSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedKhataId = khatasList.get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        progressDialog.dismiss();
        finish();
        return;
    }

    public class SearchByAsyncTasks extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            return "Ok";
        }

        @Override
        protected void onPostExecute(String s) {
            final ArrayList<String> searchBysList = new ArrayList<String>() {
                {
                    add("--منتخب کریں--");
                    add("نام");
                    add("کھاتہ");
                }
            };
            //adapter = new ArrayAdapter<String>(InputSelectionActivity.this,
              //      android.R.layout.simple_spinner_dropdown_item,
                //    searchBysList);
            try {
                //adapter.setDropDownViewResource(android.R.layout.select_dialog_singlechoice);
                //searchBySpinner.setAdapter(adapter);
                customSearchSpinnerAdapter = new CustomSpinnerAdapter(InputSelectionActivity.this, searchBysList);
                searchBySpinner.setAdapter(customSearchSpinnerAdapter);
            } catch (Exception e) {
                Log.d("Error: ", e.getMessage());
            }

            searchBySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    selectedSearchByIndex = position;
                    if (position != 0) {
                        searchBy = searchBysList.get(position);
                        if (position == 2) {
                            khataSpinner.setVisibility(View.VISIBLE);
                            loadKhataData();
                        } else if (position == 1) {
                            khataSpinner.setVisibility(View.GONE);
                            if (selectedMozaId.equals("0") || selectedMozaId.length() == 0) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(InputSelectionActivity.this);
                                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                    }
                                });
                                builder.setMessage("Select Moza before proceeding with search");
                                AlertDialog dialog = builder.create();
                                dialog.show();
                                searchBySpinner.setSelection(0);
                            } else {
                                Intent searchNameIntent = new Intent(getBaseContext(), InputnameActivity.class);
                                searchNameIntent.putExtra("mozaId", selectedMozaId);
                                startActivity(searchNameIntent);
                            }
                        }
                    } else {
                        khataSpinner.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }

    private void loadDistrictData() {
        String url = urls.DISTRICTS_URL;
        progressDialog.show();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            ArrayList<String> districtNamesList = new ArrayList<String>();
                            JSONArray dataArray = response.getJSONArray("Result");

                            District dZero = new District();
                            dZero.setId("0");
                            dZero.setName("--ضلع کاانتخاب کریں--");
                            districtsList.add(dZero);
                            districtNamesList.add(dZero.getName());

                            for (int i = 0; i < dataArray.length(); i++) {
                                try {
                                    District district = new District();
                                    JSONObject obj = dataArray.getJSONObject(i);
                                    // Pulling items from the array
                                    district.setId(obj.getString("Id"));
                                    district.setName(obj.getString("DistrictName"));
                                    districtsList.add(district);
                                    districtNamesList.add(obj.optString("DistrictName"));
                                } catch (JSONException e) {
                                    Log.d("Error: ", e.getMessage());
                                }
                            }
                            //districtAdapter.setDropDownViewResource(android.R.layout.select_dialog_singlechoice);
                            //districtSpinner.setAdapter(districtAdapter);
                            customDistrictSpinnerAdapter = new CustomSpinnerAdapter(InputSelectionActivity.this, districtNamesList);
                            districtSpinner.setAdapter(customDistrictSpinnerAdapter);
                        }catch (JSONException e) {
                            Log.d("Error: ", e.getMessage());
                        }
                        progressDialog.hide();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(InputSelectionActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                });
        //add request to queue
        requestQueue.add(jsonObjectRequest);
    }


    private void loadTehsilData() {
        String url = urls.TEHSILS_URL + selectedDistrictId;
        progressDialog.show();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            ArrayList<String> tehsilNamesList = new ArrayList<String>();
                            //tehsilAdapter = new ArrayAdapter<String>(InputSelectionActivity.this,
                              //      android.R.layout.simple_spinner_dropdown_item,
                                //    tehsilNamesList);
                            JSONArray dataArray = response.getJSONArray("Result");
                            //Log.d("json obj", response.toString());

                            tehsilsList.clear();
                            Tehsil tZero = new Tehsil();
                            tZero.setId("0");
                            tZero.setName("--تحصیل کاانتخاب کریں--");
                            tehsilsList.add(tZero);
                            tehsilNamesList.add(tZero.getName());

                            for (int i = 0; i < dataArray.length(); i++) {
                                try {
                                    Tehsil tehsil = new Tehsil();
                                    JSONObject obj = dataArray.getJSONObject(i);
                                    // Pulling items from the array
                                    tehsil.setId(obj.getString("Id"));
                                    tehsil.setName(obj.getString("TehsilName"));
                                    tehsilsList.add(tehsil);
                                    tehsilNamesList.add(obj.optString("TehsilName"));
                                } catch (JSONException e) {
                                    Log.d("Error: ", e.getMessage());
                                }
                            }
                            //tehsilAdapter.setDropDownViewResource(android.R.layout.select_dialog_singlechoice);
                            //tehsilSpinner.setAdapter(tehsilAdapter);
                            customTehsilSpinnerAdapter = new CustomSpinnerAdapter(InputSelectionActivity.this, tehsilNamesList);
                            tehsilSpinner.setAdapter(customTehsilSpinnerAdapter);
                        }catch (JSONException e) {
                            Log.d("Error: ", e.getMessage());
                        }
                        progressDialog.hide();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        //add request to queue
        requestQueue.add(jsonObjectRequest);
    }

    private void loadMozaData() {
        String url = urls.MOZAS_URL + selectedTehsilId;
        progressDialog.show();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            int mozaCounter = 0;
                            ArrayList<String> mozaNamesList = new ArrayList<String>();
                            //mozaAdapter = new ArrayAdapter<String>(InputSelectionActivity.this,
                              //      android.R.layout.simple_spinner_dropdown_item,
                                //    mozaNamesList);
                            JSONArray dataArray = response.getJSONArray("Result");
                            //Log.d("json obj", response.toString());

                            mozasList.clear();
                            Moza mZero = new Moza();
                            mZero.setId("0");
                            mZero.setName("--موضع کاانتخاب کریں--");
                            mZero.setIsLive(true);
                            mozasList.add(mZero);
                            mozaNamesList.add(mZero.getName());

                            for (int i = 0; i < dataArray.length(); i++) {
                                try {
                                    ++mozaCounter;
                                    Moza moza = new Moza();
                                    JSONObject obj = dataArray.getJSONObject(i);
                                    // Pulling items from the array
                                    moza.setId(obj.getString("Id"));
                                    moza.setName(mozaCounter + "- " + obj.getString("MozaName"));
                                    moza.setIsLive(obj.getBoolean("IsLive"));
                                    mozasList.add(moza);
                                    mozaNamesList.add(mozaCounter + "- " + obj.optString("MozaName"));
                                } catch (JSONException e) {
                                    Log.d("Error: ", e.getMessage());
                                }
                            }
                            //mozaAdapter.setDropDownViewResource(android.R.layout.select_dialog_singlechoice);
                            //mozaSpinner.setAdapter(mozaAdapter);
                            customMozaSpinnerAdapter = new CustomSpinnerMozaAdapter(InputSelectionActivity.this, mozasList);
                            mozaSpinner.setAdapter(customMozaSpinnerAdapter);
                        }catch (JSONException e) {
                            Log.d("Error: ", e.getMessage());
                        }
                        progressDialog.hide();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        //add request to queue
        requestQueue.add(jsonObjectRequest);
    }

    private void loadKhataData() {
        String url = urls.KHATAS_URL + selectedMozaId;
        progressDialog.show();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ArrayList<String> khataNamesList = new ArrayList<String>();
                        try {
                            //khataAdapter = new ArrayAdapter<String>(InputSelectionActivity.this,
                              //      android.R.layout.simple_spinner_dropdown_item,
                                //    khataNamesList);
                            JSONArray dataArray = response.getJSONArray("Result");
                            //Log.d("json obj", response.toString());

                            khatasList.clear();
                            Khata kZero = new Khata();
                            kZero.setId("0");
                            kZero.setKhata("");
                            khatasList.add(kZero);
                            khataNamesList.add("--کھاتہ منتخب کریں--");
                            for (int i = 0; i < dataArray.length(); i++) {

                                Khata khata = new Khata();
                                JSONObject obj = dataArray.getJSONObject(i);
                                // Pulling items from the array
                                khata.setId(obj.getString("Id"));
                                String khataNo = obj.getString("KhataNo");
                                khata.setKhata(khataNo);
                                khatasList.add(khata);
                                khataNamesList.add(khataNo);
                            }
                        }catch (JSONException e) {
                            Log.d("Error: ", e.getMessage());
                        }
                        //khataAdapter.setDropDownViewResource(android.R.layout.select_dialog_singlechoice);
                        //khataSpinner.setAdapter(khataAdapter);
                        customKhataSpinnerAdapter = new CustomSpinnerAdapter(InputSelectionActivity.this, khataNamesList);
                        khataSpinner.setAdapter(customKhataSpinnerAdapter);
                        progressDialog.hide();
                        khataSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                selectedKhataId = khatasList.get(position).getId();
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        //add request to queue
        requestQueue.add(jsonObjectRequest);
    }

    public class CustomSpinnerAdapter extends BaseAdapter implements SpinnerAdapter {

        private final Context activity;
        private ArrayList<String> asr;

        public CustomSpinnerAdapter(Context context, ArrayList<String> asr) {
            this.asr=asr;
            activity = context;
        }

        public int getCount() {
            return asr.size();
        }

        public Object getItem(int i) {
            return asr.get(i);
        }

        public long getItemId(int i) {
            return (long)i;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            TextView txt = new TextView(InputSelectionActivity.this);
            txt.setPadding(16, 16, 25, 16);
            txt.setTextSize(20);
            txt.setTypeface(appTextTypeFace, Typeface.BOLD);

            //txt.setGravity(Gravity.CENTER_VERTICAL);
            txt.setText(asr.get(position));
            txt.setTextDirection(View.TEXT_DIRECTION_RTL);
            txt.setTextColor(getResources().getColor(R.color.dropDownTextColor));
            //txt.setCheckMarkDrawable(android.R.drawable.btn_radio);
            return  txt;
        }

        public View getView(int i, View view, ViewGroup viewgroup) {
            TextView txt = new TextView(InputSelectionActivity.this);
            //txt.setGravity(Gravity.RIGHT);
            txt.setPadding(16, 16, 25, 16);
            txt.setTextSize(20);
            txt.setTypeface(appTextTypeFace, Typeface.BOLD);
            txt.setText(asr.get(i));
            txt.setTextColor(getResources().getColor(R.color.dropDownTextColor));
            txt.setTextDirection(View.TEXT_DIRECTION_RTL);
            return  txt;
        }

    }

    public class CustomSpinnerMozaAdapter extends BaseAdapter implements SpinnerAdapter {

        private final Context activity;
        private ArrayList<Moza> mozasObjectsList;

        public CustomSpinnerMozaAdapter(Context context, ArrayList<Moza> list) {
            this.mozasObjectsList = list;
            activity = context;
        }

        public int getCount() {
            return mozasObjectsList.size();
        }

        public Object getItem(int i) {
            return mozasObjectsList.get(i);
        }

        public long getItemId(int i) {
            return (long)i;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            TextView txt = new TextView(InputSelectionActivity.this);
            txt.setPadding(16, 16, 25, 16);
            txt.setTextSize(20);
            txt.setTypeface(appTextTypeFace, Typeface.BOLD);

            Moza mozaObject = this.mozasObjectsList.get(position);
            String mozaName = mozaObject.getName();
            boolean isLive = mozaObject.getIsLive();
            //txt.setGravity(Gravity.CENTER_VERTICAL);
            if (isLive) {
                txt.setTextColor(getResources().getColor(R.color.dropDownTextColor));
            } else {
                txt.setTextColor(getResources().getColor(R.color.red));
            }
            txt.setText(mozaName);
            txt.setTextDirection(View.TEXT_DIRECTION_RTL);
            //txt.setTextColor(getResources().getColor(R.color.dropDownTextColor));
            //txt.setCheckMarkDrawable(android.R.drawable.btn_radio);
            return  txt;
        }

        public View getView(int i, View view, ViewGroup viewgroup) {
            Moza mozaObject = mozasObjectsList.get(i);
            boolean isLive = mozaObject.getIsLive();
            TextView txt = new TextView(InputSelectionActivity.this);
            //txt.setGravity(Gravity.RIGHT);
            txt.setPadding(16, 16, 25, 16);
            txt.setTextSize(20);
            txt.setTypeface(appTextTypeFace, Typeface.BOLD);
            txt.setText(mozaObject.getName());

            if (!isLive) {
                txt.setTextColor(getResources().getColor(R.color.red));
            } else {
                txt.setTextColor(getResources().getColor(R.color.dropDownTextColor));
            }

            txt.setTextDirection(View.TEXT_DIRECTION_RTL);
            return  txt;
        }

    }
}
