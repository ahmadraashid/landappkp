package pmu.bor.landappkp;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import me.anwarshahriar.calligrapher.Calligrapher;
import pmu.bor.landappkp.helpers.Settings;

public class FardbyNICActivity extends AppCompatActivity {

    Button btnSearch;
    EditText txtnic;
    int nicLength = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fardby_nic);

        Settings settings = new Settings();
        Calligrapher calligrapher = new Calligrapher(this);
        calligrapher.setFont(this, settings.getUrduFontPath(), true);

        btnSearch = (Button) findViewById(R.id.btn_search);
        txtnic = (EditText) findViewById(R.id.txt_nic);
        nicLength = settings.getNICLength();

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nic = txtnic.getText().toString();
                if (nic.length() < nicLength) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(FardbyNICActivity.this);
                    builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    });

                    builder.setMessage(R.string.full_nic_required);
                    AlertDialog dialog = builder.create();
                    dialog.show();
                } else {
                    Intent intent = new Intent(v.getContext(), UserMalkiatActivity.class);
                    intent.putExtra("searchCriteria", nic);
                    intent.putExtra("searchType", "2");
                    startActivity(intent);
                }
            }
        });
    }
}
