package pmu.bor.landappkp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import me.anwarshahriar.calligrapher.Calligrapher;
import pmu.bor.landappkp.helpers.EngToUrduCharMap;
import pmu.bor.landappkp.helpers.Settings;
import pmu.bor.landappkp.helpers.URLStore;
import pmu.bor.landappkp.models.District;
import pmu.bor.landappkp.models.Khata;
import pmu.bor.landappkp.models.Moza;
import pmu.bor.landappkp.models.Tehsil;

public class MutationByNoActivity extends AppCompatActivity {

    private Spinner districtSpinner;
    private Spinner tehsilSpinner;
    private Spinner mozaSpinner;
    Button btnFind;
    EditText txtIntiqalNo;

    JSONObject jsonobject;
    JSONArray jsonarray;
    ArrayList<District> districtsList;
    ArrayList<Tehsil> tehsilsList;
    ArrayList<Moza> mozasList;
    ArrayList<Khata> khatasList;
    ProgressDialog progressDialog;
    MutationByNoActivity.CustomSpinnerAdapter customDistrictSpinnerAdapter;
    MutationByNoActivity.CustomSpinnerAdapter customTehsilSpinnerAdapter;
    MutationByNoActivity.CustomSpinnerMozaAdapter customMozaSpinnerAdapter;
    //MutationByNoActivity.CustomSpinnerAdapter customKhataSpinnerAdapter;
    MutationByNoActivity.CustomSpinnerAdapter customSearchSpinnerAdapter;
    ArrayAdapter<String> adapter;
    ArrayAdapter<String> districtAdapter;
    ArrayAdapter<String> tehsilAdapter;
    ArrayAdapter<String> mozaAdapter;
    ArrayAdapter<String> khataAdapter;
    String selectedDistrictId = "";
    String selectedTehsilId = "";
    String selectedMozaId = "";
    String searchBy = "";
    String selectedKhataId = "";
    Typeface appTextTypeFace;
    //String selectedName = "";
    //String selectedFatherName = "";
    URLStore urls;
    RequestQueue requestQueue;
    ArrayList<String> emptyList;
    int mozaSerial = 0;
    ArrayList<Moza> emptyMozasList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mutation_byno);

        Settings settings = new Settings();
        Calligrapher calligrapher = new Calligrapher(this);
        calligrapher.setFont(this, settings.getUrduFontPath(), true);

        appTextTypeFace = Typeface.createFromAsset(getAssets(), settings.getUrduFontPath());

        emptyList = new ArrayList<String>();
        emptyMozasList = new ArrayList<Moza>();
        requestQueue = Volley.newRequestQueue(this);
        districtSpinner = (Spinner) findViewById(R.id.district_spinner);
        tehsilSpinner = (Spinner) findViewById(R.id.tehsil_spinner);
        mozaSpinner = (Spinner) findViewById(R.id.moza_spinner);

        btnFind = (Button) findViewById(R.id.btn_find);
        txtIntiqalNo = (EditText) findViewById(R.id.txt_intiqalno);

        urls = new URLStore();
        districtsList = new ArrayList<District>();
        tehsilsList = new ArrayList<Tehsil>();
        mozasList = new ArrayList<Moza>();
        khatasList = new ArrayList<Khata>();
        progressDialog = new ProgressDialog(MutationByNoActivity.this);
        progressDialog.setMessage("Please Wait");
        progressDialog.setCancelable(false);
        final EngToUrduCharMap charMapper = new EngToUrduCharMap();
        loadDistrictData();

        btnFind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                String intiqalNo = txtIntiqalNo.getText().toString();
                AlertDialog.Builder builder = new AlertDialog.Builder(MutationByNoActivity.this);
                // Add the buttons
                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User clicked OK button
                    }
                });

                if (selectedMozaId.length() == 0 || selectedMozaId.equals("0")) {
                    builder.setMessage(R.string.moza_required);
                    AlertDialog dialog = builder.create();
                    dialog.show();
                } else if (intiqalNo.length() == 0){
                    builder.setMessage(R.string.intiqal_no_required);
                    AlertDialog dialog = builder.create();
                    dialog.show();
                } else {
                    intent = new Intent(v.getContext(), MutationResultActivity.class);
                    intent.putExtra("mozaId", selectedMozaId);
                    intent.putExtra("intiqalNo", intiqalNo);
                    intent.putExtra("searchBy", "no");
                    startActivity(intent);
                }
            }
        });

        //Drop down selections handling

        districtSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    selectedDistrictId = districtsList.get(position).getId();
                    loadTehsilData();
                } else {
                    selectedDistrictId = "0";
                    customTehsilSpinnerAdapter = new MutationByNoActivity.CustomSpinnerAdapter(MutationByNoActivity.this, emptyList);
                    tehsilSpinner.setAdapter(customTehsilSpinnerAdapter);
                }

                customMozaSpinnerAdapter = new MutationByNoActivity.CustomSpinnerMozaAdapter(MutationByNoActivity.this, emptyMozasList);
                mozaSpinner.setAdapter(customMozaSpinnerAdapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        //Tehsils Spinner
        tehsilSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    selectedTehsilId = tehsilsList.get(position).getId();

                    loadMozaData();
                } else {
                    selectedTehsilId = "0";
                    selectedMozaId = "0";
                    customMozaSpinnerAdapter = new MutationByNoActivity.CustomSpinnerMozaAdapter(MutationByNoActivity.this, emptyMozasList);
                    mozaSpinner.setAdapter(customMozaSpinnerAdapter);
                }
                //customKhataSpinnerAdapter = new MutationByNoActivity.CustomSpinnerAdapter(MutationByNoActivity.this, emptyList);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        //Moza Spinner
        mozaSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    selectedMozaId = mozasList.get(position).getId();
                } else {
                    selectedMozaId = "0";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void loadDistrictData() {
        String url = urls.DISTRICTS_URL;
        progressDialog.show();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            ArrayList<String> districtNamesList = new ArrayList<String>();
                            JSONArray dataArray = response.getJSONArray("Result");

                            District dZero = new District();
                            dZero.setId("0");
                            dZero.setName("--ضلع کاانتخاب کریں--");
                            districtsList.add(dZero);
                            districtNamesList.add(dZero.getName());

                            for (int i = 0; i < dataArray.length(); i++) {
                                try {
                                    District district = new District();
                                    JSONObject obj = dataArray.getJSONObject(i);
                                    // Pulling items from the array
                                    district.setId(obj.getString("Id"));
                                    district.setName(obj.getString("DistrictName"));
                                    districtsList.add(district);
                                    districtNamesList.add(obj.optString("DistrictName"));
                                } catch (JSONException e) {
                                    Log.d("Error: ", e.getMessage());
                                }
                            }
                            //districtAdapter.setDropDownViewResource(android.R.layout.select_dialog_singlechoice);
                            //districtSpinner.setAdapter(districtAdapter);
                            customDistrictSpinnerAdapter = new MutationByNoActivity.CustomSpinnerAdapter(MutationByNoActivity.this, districtNamesList);
                            districtSpinner.setAdapter(customDistrictSpinnerAdapter);
                        } catch (JSONException e) {
                            Log.d("Error: ", e.getMessage());
                        }
                        progressDialog.hide();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MutationByNoActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                });
        //add request to queue
        requestQueue.add(jsonObjectRequest);
    }


    private void loadTehsilData() {
        String url = urls.TEHSILS_URL + selectedDistrictId;
        progressDialog.show();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            ArrayList<String> tehsilNamesList = new ArrayList<String>();
                            //tehsilAdapter = new ArrayAdapter<String>(MutationByNoActivity.this,
                            //      android.R.layout.simple_spinner_dropdown_item,
                            //    tehsilNamesList);
                            JSONArray dataArray = response.getJSONArray("Result");
                            //Log.d("json obj", response.toString());

                            tehsilsList.clear();
                            Tehsil tZero = new Tehsil();
                            tZero.setId("0");
                            tZero.setName("--تحصیل کاانتخاب کریں--");
                            tehsilsList.add(tZero);
                            tehsilNamesList.add(tZero.getName());

                            for (int i = 0; i < dataArray.length(); i++) {
                                try {
                                    Tehsil tehsil = new Tehsil();
                                    JSONObject obj = dataArray.getJSONObject(i);
                                    // Pulling items from the array
                                    tehsil.setId(obj.getString("Id"));
                                    tehsil.setName(obj.getString("TehsilName"));
                                    tehsilsList.add(tehsil);
                                    tehsilNamesList.add(obj.optString("TehsilName"));
                                } catch (JSONException e) {
                                    Log.d("Error: ", e.getMessage());
                                }
                            }
                            //tehsilAdapter.setDropDownViewResource(android.R.layout.select_dialog_singlechoice);
                            //tehsilSpinner.setAdapter(tehsilAdapter);
                            customTehsilSpinnerAdapter = new MutationByNoActivity.CustomSpinnerAdapter(MutationByNoActivity.this, tehsilNamesList);
                            tehsilSpinner.setAdapter(customTehsilSpinnerAdapter);
                        } catch (JSONException e) {
                            Log.d("Error: ", e.getMessage());
                        }
                        progressDialog.hide();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        //add request to queue
        requestQueue.add(jsonObjectRequest);
    }

    private void loadMozaData() {
        String url = urls.MOZAS_URL + selectedTehsilId;
        progressDialog.show();
        mozaSerial = 0;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            ArrayList<String> mozaNamesList = new ArrayList<String>();
                            //mozaAdapter = new ArrayAdapter<String>(MutationByNoActivity.this,
                            //      android.R.layout.simple_spinner_dropdown_item,
                            //    mozaNamesList);
                            JSONArray dataArray = response.getJSONArray("Result");
                            Log.d("json obj", response.toString());

                            mozasList.clear();
                            Moza mZero = new Moza();
                            mZero.setId("0");
                            mZero.setIsLive(true);
                            mZero.setName("--موضع کاانتخاب کریں--");
                            mozasList.add(mZero);
                            mozaNamesList.add(mZero.getName());

                            for (int i = 0; i < dataArray.length(); i++) {
                                try {
                                    ++mozaSerial;
                                    Moza moza = new Moza();
                                    JSONObject obj = dataArray.getJSONObject(i);
                                    // Pulling items from the array
                                    moza.setIsLive(obj.getBoolean("IsLive"));
                                    moza.setId(obj.getString("Id"));
                                    moza.setName(mozaSerial + "- " + obj.getString("MozaName"));
                                    mozasList.add(moza);
                                    mozaNamesList.add(mozaSerial + "- " + obj.optString("MozaName"));
                                } catch (JSONException e) {
                                    Log.d("Error: ", e.getMessage());
                                }
                            }
                            //mozaAdapter.setDropDownViewResource(android.R.layout.select_dialog_singlechoice);
                            //mozaSpinner.setAdapter(mozaAdapter);
                            customMozaSpinnerAdapter = new MutationByNoActivity.CustomSpinnerMozaAdapter(MutationByNoActivity.this, mozasList);
                            mozaSpinner.setAdapter(customMozaSpinnerAdapter);
                        } catch (JSONException e) {
                            Log.d("Error: ", e.getMessage());
                        }
                        progressDialog.hide();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        //add request to queue
        requestQueue.add(jsonObjectRequest);
    }

    public class CustomSpinnerAdapter extends BaseAdapter implements SpinnerAdapter {

        private final Context activity;
        private ArrayList<String> asr;

        public CustomSpinnerAdapter(Context context, ArrayList<String> asr) {
            this.asr = asr;
            activity = context;
        }

        public int getCount() {
            return asr.size();
        }

        public Object getItem(int i) {
            return asr.get(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            TextView txt = new TextView(MutationByNoActivity.this);
            txt.setPadding(16, 16, 25, 16);
            txt.setTextSize(20);
            txt.setTypeface(appTextTypeFace, Typeface.BOLD);

            //txt.setGravity(Gravity.CENTER_VERTICAL);
            txt.setText(asr.get(position));
            txt.setTextDirection(View.TEXT_DIRECTION_RTL);
            txt.setTextColor(Color.parseColor("#70000000"));
            //txt.setCheckMarkDrawable(android.R.drawable.btn_radio);
            return txt;
        }

        public View getView(int i, View view, ViewGroup viewgroup) {
            TextView txt = new TextView(MutationByNoActivity.this);
            //txt.setGravity(Gravity.RIGHT);
            txt.setPadding(16, 16, 25, 16);
            txt.setTextSize(20);
            txt.setTypeface(appTextTypeFace, Typeface.BOLD);
            txt.setText(asr.get(i));
            txt.setTextColor(Color.parseColor("#70000000"));
            txt.setTextDirection(View.TEXT_DIRECTION_RTL);
            return txt;
        }

    }

    public class CustomSpinnerMozaAdapter extends BaseAdapter implements SpinnerAdapter {

        private final Context activity;
        private ArrayList<Moza> mozasObjectsList;

        public CustomSpinnerMozaAdapter(Context context, ArrayList<Moza> list) {
            this.mozasObjectsList = list;
            activity = context;
        }

        public int getCount() {
            return mozasObjectsList.size();
        }

        public Object getItem(int i) {
            return mozasObjectsList.get(i);
        }

        public long getItemId(int i) {
            return (long)i;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            TextView txt = new TextView(MutationByNoActivity.this);
            txt.setPadding(16, 16, 25, 16);
            txt.setTextSize(20);
            txt.setTypeface(appTextTypeFace, Typeface.BOLD);

            Moza mozaObject = this.mozasObjectsList.get(position);
            String mozaName = mozaObject.getName();
            boolean isLive = mozaObject.getIsLive();
            //txt.setGravity(Gravity.CENTER_VERTICAL);
            if (isLive) {
                txt.setTextColor(getResources().getColor(R.color.dropDownTextColor));
            } else {
                txt.setTextColor(getResources().getColor(R.color.red));
            }
            txt.setText(mozaName);
            txt.setTextDirection(View.TEXT_DIRECTION_RTL);
            //txt.setTextColor(getResources().getColor(R.color.dropDownTextColor));
            //txt.setCheckMarkDrawable(android.R.drawable.btn_radio);
            return  txt;
        }

        public View getView(int i, View view, ViewGroup viewgroup) {
            Moza mozaObject = mozasObjectsList.get(i);
            boolean isLive = mozaObject.getIsLive();
            TextView txt = new TextView(MutationByNoActivity.this);
            //txt.setGravity(Gravity.RIGHT);
            txt.setPadding(16, 16, 25, 16);
            txt.setTextSize(20);
            txt.setTypeface(appTextTypeFace, Typeface.BOLD);
            txt.setText(mozaObject.getName());

            if (!isLive) {
                txt.setTextColor(getResources().getColor(R.color.red));
            } else {
                txt.setTextColor(getResources().getColor(R.color.dropDownTextColor));
            }

            txt.setTextDirection(View.TEXT_DIRECTION_RTL);
            return  txt;
        }

    }
}
