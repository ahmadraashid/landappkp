package pmu.bor.landappkp;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.renderscript.ScriptGroup;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import me.anwarshahriar.calligrapher.Calligrapher;
import pmu.bor.landappkp.helpers.Settings;

public class MainActivity extends AppCompatActivity {

    Button btnFard;
    Button btnIntiqal;
    TextView txtWelcome;
    ImageView logo;
    int selectedOptionForIntiqal = 0;
    Typeface appFontFace;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Settings settings = new Settings();
        Calligrapher calligrapher = new Calligrapher(this);
        calligrapher.setFont(this, settings.getUrduFontPath(), true);
        appFontFace = Typeface.createFromAsset(getAssets(), settings.getUrduFontPath());
        btnFard = (Button) findViewById(R.id.btn_fard);
        btnIntiqal = (Button) findViewById(R.id.btn_intiqal);
        logo = (ImageView) findViewById(R.id.img_logo);
        txtWelcome = (TextView) findViewById(R.id.txt_welcome);
        selectedOptionForIntiqal = 0;
        //Typeface typeface = Typeface.createFromAsset(getAssets(), "Jameel_Noori_Nastaleeq.ttf");
        //txtWelcome.setTypeface(typeface);
        //btnFard.setTypeface(typeface);

        /*char ch = 'ر';
        int uniCode = (int)ch;
        Log.d("Unicode", "The unicode is: " + uniCode);
        char cch = (char)uniCode;
        Log.d("Character", "Converted character is: " + cch);*/

        //String str = "ح" + "ا" + "ک" + "م";
        //Log.d("Urdu String is: ", str);

        btnFard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), InputSelectionActivity.class);
                startActivityForResult(intent, 0);
            }
        });

        btnIntiqal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSearchyByForIntiqal();
            }
        });
    }


    private void showSearchyByForIntiqal() {
        // setup the alert builder
        selectedOptionForIntiqal = 0;
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(getResources().getString(R.string.select_an_option));

        String byNo = getResources().getString(R.string.searchby_intiqalno);
        String byToken = getResources().getString(R.string.searchby_token);

        // add a radio button list
        final String[] options = { byNo, byToken};
        int checkedItem = 0;
        builder.setSingleChoiceItems(options, checkedItem, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                selectedOptionForIntiqal = which;
            }
        });

        // add OK and Cancel buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent;

                if (selectedOptionForIntiqal == 0) {
                    intent = new Intent(getBaseContext(), MutationByNoActivity.class);
                    startActivity(intent);
                } else {
                    intent = new Intent(getBaseContext(), MutationByTokenActivity.class);
                    startActivity(intent);
                }
            }
        });
        builder.setNegativeButton("Cancel", null);
        TextView tv_message = new TextView(this);
        tv_message.setTypeface(appFontFace);
        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

}
