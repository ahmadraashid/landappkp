package pmu.bor.landappkp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import me.anwarshahriar.calligrapher.Calligrapher;
import pmu.bor.landappkp.helpers.Settings;
import pmu.bor.landappkp.helpers.URLStore;
import pmu.bor.landappkp.models.District;
import pmu.bor.landappkp.models.Result;

public class ResultActivity extends AppCompatActivity {

    ProgressDialog progressDialog;
    ListView listView;
    TextView txtNoRecordMessage;
    LinearLayout layout_norecord;
    URLStore urls;
    String selectedKhataId = "";
    ArrayList<Result> userResults;
    ArrayList<String> results;
    Typeface appFontFace;
    ArrayAdapter<Result> resultArrayAdapter;
    RequestQueue requestQueue;
    int serialCounter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        Settings settings = new Settings();
        Calligrapher calligrapher = new Calligrapher(this);
        calligrapher.setFont(this, settings.getUrduFontPath(), true);
        appFontFace = Typeface.createFromAsset(getAssets(), settings.getUrduFontPath());

        serialCounter = 0;
        requestQueue = Volley.newRequestQueue(this);
        listView = (ListView) findViewById(R.id.lv_result);
        txtNoRecordMessage = (TextView) findViewById(R.id.lbl_norecord);
        layout_norecord = (LinearLayout) findViewById(R.id.layout_norecord);
        //txtNoRecordMessage.setVisibility(View.GONE);
        layout_norecord.setVisibility(View.GONE);

        userResults = new ArrayList<Result>();
        urls = new URLStore();
        selectedKhataId = getIntent().getStringExtra("khataId");
        loadResult();
    }

    class ResultAdapter extends ArrayAdapter<Result> {
        private Context context;
        List<Result> dataResults;
        int totalRecords = 0;

        public ResultAdapter(Context context, int resource, List<Result> objects) {
            super(context, resource, objects);

            this.context = context;
            this.dataResults = objects;
            this.totalRecords = objects.size();
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            Result resultObj = userResults.get(position);
            String serial = String.valueOf(resultObj.getSerial());
            String name = resultObj.getName();
            String part = resultObj.getPart();
            String totalPart = resultObj.getTotalPart();
            String arr[] = totalPart.split("-");
            String totalPartStr = "";

            if (arr.length > 0) {
                for(int i=0; i < arr.length; i++) {
                    switch (i) {
                        case 0:
                            totalPartStr = arr[i] + " کنال ";
                            break;

                        case 1:
                            totalPartStr += arr[i] + " مرلہ ";
                            break;

                        case 2:
                            totalPartStr += arr[i] + " فٹ ";
                            break;
                    }
                }
            }

            //get the inflater and inflate the XML layout for each item
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            //conditionally inflate either standard or special template
            View view;
            view = inflater.inflate(R.layout.list_item_layout, null);

            TextView txtSerialLabel = (TextView) view.findViewById(R.id.txtSerialLabel);
            TextView txtNameLbl = (TextView) view.findViewById(R.id.txtNameLabel);
            TextView txtPartLbl = (TextView) view.findViewById(R.id.txtPartLabel);
            TextView txtTotalPartLbl = (TextView) view.findViewById(R.id.txtTotalPartLabel);
            txtSerialLabel.setTypeface(appFontFace, Typeface.BOLD);
            txtNameLbl.setTypeface(appFontFace, Typeface.BOLD);
            txtPartLbl.setTypeface(appFontFace, Typeface.BOLD);
            txtTotalPartLbl.setTypeface(appFontFace, Typeface.BOLD);

            TextView txtSerial = (TextView) view.findViewById(R.id.txtSerial);
            TextView txtName = (TextView) view.findViewById(R.id.txtName);
            TextView txtPart = (TextView) view.findViewById(R.id.txtPart);
            TextView txtTotalPart = (TextView) view.findViewById(R.id.txtTotalPart);
            txtSerial.setTypeface(appFontFace);
            txtName.setTypeface(appFontFace);
            txtPart.setTypeface(appFontFace);
            txtTotalPart.setTypeface(appFontFace);

            String serialStr = serial + "/" + totalRecords;
            txtSerial.setText(serialStr);
            txtName.setText(name);
            txtPart.setText(part);
            txtTotalPart.setText(totalPartStr);

            return view;
        }
    }

    private void loadResult() {
        String url = urls.LAND_OWNERS_URL + selectedKhataId;
        progressDialog = new ProgressDialog(ResultActivity.this);
        progressDialog.setMessage("Please Wait");
        progressDialog.setCancelable(false);
        progressDialog.show();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        results = new ArrayList<String>();
                        try {
                                JSONArray jsonArray = response.getJSONArray("Result");
                                if (jsonArray.length() == 0) {
                                    listView.setVisibility(View.GONE);
                                    //txtNoRecordMessage.setVisibility(View.VISIBLE);
                                    layout_norecord.setVisibility(View.VISIBLE);
                                } else {
                                    JSONObject oneObject = jsonArray.getJSONObject(0);
                                    // display the data in UI
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        try {
                                            ++serialCounter;
                                            String data = "";
                                            JSONObject obj = jsonArray.getJSONObject(i);

                                            Result resultObj = new Result();
                                            resultObj.setSerial(serialCounter);
                                            resultObj.setName(obj.getString("PersonName"));
                                            resultObj.setPart(obj.getString("NetPart"));
                                            resultObj.setTotalPart(obj.getString("KhewatArea"));
                                            userResults.add(resultObj);
                                            //adapter.add(data);
                                        } catch (JSONException e) {
                                            Log.d("Error: ", e.getMessage());
                                        }
                                    }
                                    resultArrayAdapter = new ResultAdapter(ResultActivity.this, 0, userResults);
                                    listView.setAdapter(resultArrayAdapter);
                                }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        progressDialog.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(),"Unable to connect", Toast.LENGTH_LONG).show();

                    }
                });
        //add request to queue
        requestQueue.add(jsonObjectRequest);
    }
}

