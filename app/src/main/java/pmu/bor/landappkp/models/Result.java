package pmu.bor.landappkp.models;

/**
 * Created by Rashid Ahmad on 2/12/2018.
 */

public class Result {
    private int serial;
    private String name;
    private String part;
    private String totalPart;

    public void setSerial(int serial) {
        this.serial = serial;
    }

    public int getSerial() {
        return this.serial;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setPart(String part) {
        this.part = part;
    }

    public String getPart() {
        return this.part;
    }

    public void setTotalPart(String totalPart) {
        this.totalPart = totalPart;
    }

    public String getTotalPart() {
        return this.totalPart;
    }
}
