package pmu.bor.landappkp.helpers;

/**
 * Created by Rashid Ahmad on 1/29/2018.
 */

public class URLStore {
    private String baseUrl = "http://175.107.63.31:9080/api/";
    public final String DISTRICTS_URL = baseUrl + "executeReader/getDistrictsJson";
    public final String TEHSILS_URL = baseUrl + "executeReader/getDistrictTehsilsJson?id=";
    public final String MOZAS_URL = baseUrl + "executeReader/getTehsilMozasJson?id=";
    public final String KHATAS_URL = baseUrl + "executeReader/getMozaKhatasJson?id=";
    public final String LAND_OWNERS_URL = baseUrl + "executeReader/getKhataMaalkanJson?id=";
    public final String MATCHING_NAMES_LIST_MOZA = baseUrl + "executeReader/retrieveAllMatchingNamesInMoza";
    public final String GET_USER_MALKIAT_BYNIC = baseUrl + "executeReader/getMalkiatByNIC?nic=";
    public final String GET_USER_MALKIAT_BYPERSONID = baseUrl + "executeReader/getMalkiatByPersonId?personId=";
    public final String GET_INTIQAL_STATUS_BY_TOKEN = baseUrl + "executeScalar/getIntiqalStatusByToken?";
    public final String GET_INTIQAL_STATUS_BY_NO = baseUrl + "executeScalar/getIntiqalStatusByIntiqalNo?";

}
