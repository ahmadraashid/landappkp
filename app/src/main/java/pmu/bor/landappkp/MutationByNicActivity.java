package pmu.bor.landappkp;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import me.anwarshahriar.calligrapher.Calligrapher;
import pmu.bor.landappkp.helpers.Settings;

public class MutationByNicActivity extends AppCompatActivity {

    Button btnFind;
    EditText txtNic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mutation_by_nic);

        Settings settings = new Settings();
        Calligrapher calligrapher = new Calligrapher(this);
        calligrapher.setFont(this, settings.getUrduFontPath(), true);

        txtNic = (EditText) findViewById(R.id.txt_nic);
        btnFind = (Button) findViewById(R.id.btn_find);

        btnFind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MutationByNicActivity.this);
                String nic = txtNic.getText().toString();
                if (nic.length() < 13) {
                    builder.setMessage(R.string.nic_required);
                    AlertDialog dialog = builder.create();
                    dialog.show();
                } else {
                    Intent intent = new Intent(v.getContext(), MutationResultActivity.class);
                    intent.putExtra("nic", nic);
                    startActivity(intent);
                }
            }
        });
    }
}
