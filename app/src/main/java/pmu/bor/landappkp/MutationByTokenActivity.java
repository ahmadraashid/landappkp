package pmu.bor.landappkp;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

import me.anwarshahriar.calligrapher.Calligrapher;
import pmu.bor.landappkp.helpers.EngToUrduCharMap;
import pmu.bor.landappkp.helpers.Settings;
import pmu.bor.landappkp.helpers.URLStore;
import pmu.bor.landappkp.models.District;
import pmu.bor.landappkp.models.Tehsil;

public class MutationByTokenActivity extends AppCompatActivity {

    private Spinner districtSpinner;
    private Spinner tehsilSpinner;
    private Calendar date;
    ProgressDialog progressDialog;
    Button btnDated;
    Button btnSearch;
    EditText txtToken;
    String dateStr = "";
    String selectedDistrictId = "";
    String selectedTehsilId = "";

    ArrayList<District> districtsList;
    ArrayList<Tehsil> tehsilsList;
    ArrayAdapter<String> districtAdapter;
    ArrayAdapter<String> tehsilAdapter;
    RequestQueue requestQueue;
    ArrayList<String> emptyList;
    Typeface appTextTypeFace;
    URLStore urls;

    MutationByTokenActivity.CustomSpinnerAdapter customDistrictSpinnerAdapter;
    MutationByTokenActivity.CustomSpinnerAdapter customTehsilSpinnerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mutation_by_token);

        Settings settings = new Settings();
        Calligrapher calligrapher = new Calligrapher(this);
        calligrapher.setFont(this, settings.getUrduFontPath(), true);

        appTextTypeFace = Typeface.createFromAsset(getAssets(), settings.getUrduFontPath());

        emptyList = new ArrayList<String>();
        requestQueue = Volley.newRequestQueue(this);
        districtSpinner = (Spinner) findViewById(R.id.district_spinner);
        tehsilSpinner = (Spinner) findViewById(R.id.tehsil_spinner);

        btnDated = (Button) findViewById(R.id.btn_dated);
        btnSearch = (Button) findViewById(R.id.btn_search);
        txtToken = (EditText) findViewById(R.id.txt_token);

        urls = new URLStore();
        districtsList = new ArrayList<District>();
        tehsilsList = new ArrayList<Tehsil>();
        progressDialog = new ProgressDialog(MutationByTokenActivity.this);
        progressDialog.setMessage("Please Wait");
        progressDialog.setCancelable(false);
        loadDistrictData();

        districtSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    selectedDistrictId = districtsList.get(position).getId();
                    loadTehsilData();
                } else {
                    selectedDistrictId = "0";
                    customTehsilSpinnerAdapter = new MutationByTokenActivity.CustomSpinnerAdapter(MutationByTokenActivity.this, emptyList);
                    tehsilSpinner.setAdapter(customTehsilSpinnerAdapter);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        //Tehsils Spinner
        tehsilSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    selectedTehsilId = tehsilsList.get(position).getId();
                } else {
                    selectedTehsilId = "0";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        btnDated.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDateTimePicker();

            }
        });

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                AlertDialog.Builder builder = new AlertDialog.Builder(MutationByTokenActivity.this);
                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });

                String dated = btnDated.getText().toString();
                String token = txtToken.getText().toString();

                if (dated.length() == 0) {
                    builder.setMessage(R.string.date_required);
                    AlertDialog dialog = builder.create();
                    dialog.show();
                } else if (token.length() == 0) {
                    builder.setMessage(R.string.token_required);
                    AlertDialog dialog = builder.create();
                    dialog.show();
                } else if(selectedTehsilId.length() == 0) {
                    builder.setMessage(R.string.tehsil_required);
                    AlertDialog dialog = builder.create();
                    dialog.show();
                } else {
                    intent = new Intent(v.getContext(), MutationResultActivity.class);
                    intent.putExtra("tokenNo", token);
                    intent.putExtra("intiqalDate", dated);
                    intent.putExtra("tehsilId", selectedTehsilId);
                    intent.putExtra("searchBy", "token");
                    startActivity(intent);
                }
            }
        });
    }


    public void showDateTimePicker() {
        final Calendar currentDate = Calendar.getInstance();
        date = Calendar.getInstance();
        new DatePickerDialog(MutationByTokenActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                date.set(year, monthOfYear, dayOfMonth);
                if (monthOfYear < 12)
                    monthOfYear++;

                dateStr =  dayOfMonth + "/" + monthOfYear + "/" + year;
                btnDated.setText(dateStr);
                /*new TimePickerDialog(MutationByTokenActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        date.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        date.set(Calendar.MINUTE, minute);
                        Log.v("Time", "The choosen one " + date.getTime());
                    }
                }, currentDate.get(Calendar.HOUR_OF_DAY), currentDate.get(Calendar.MINUTE), false).show();*/
            }
        }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DATE)).show();
    }

    private void loadDistrictData() {
        String url = urls.DISTRICTS_URL;
        progressDialog.show();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            ArrayList<String> districtNamesList = new ArrayList<String>();
                            JSONArray dataArray = response.getJSONArray("Result");

                            District dZero = new District();
                            dZero.setId("0");
                            dZero.setName("--ضلع کاانتخاب کریں--");
                            districtsList.add(dZero);
                            districtNamesList.add(dZero.getName());

                            for (int i = 0; i < dataArray.length(); i++) {
                                try {
                                    District district = new District();
                                    JSONObject obj = dataArray.getJSONObject(i);
                                    // Pulling items from the array
                                    district.setId(obj.getString("Id"));
                                    district.setName(obj.getString("DistrictName"));
                                    districtsList.add(district);
                                    districtNamesList.add(obj.optString("DistrictName"));
                                } catch (JSONException e) {
                                    Log.d("Error: ", e.getMessage());
                                }
                            }
                            //districtAdapter.setDropDownViewResource(android.R.layout.select_dialog_singlechoice);
                            //districtSpinner.setAdapter(districtAdapter);
                            customDistrictSpinnerAdapter = new MutationByTokenActivity.CustomSpinnerAdapter(MutationByTokenActivity.this, districtNamesList);
                            districtSpinner.setAdapter(customDistrictSpinnerAdapter);
                        } catch (JSONException e) {
                            Log.d("Error: ", e.getMessage());
                        }
                        progressDialog.hide();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MutationByTokenActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                });
        //add request to queue
        requestQueue.add(jsonObjectRequest);
    }


    private void loadTehsilData() {
        String url = urls.TEHSILS_URL + selectedDistrictId;
        progressDialog.show();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            ArrayList<String> tehsilNamesList = new ArrayList<String>();
                            //tehsilAdapter = new ArrayAdapter<String>(MutationByNoActivity.this,
                            //      android.R.layout.simple_spinner_dropdown_item,
                            //    tehsilNamesList);
                            JSONArray dataArray = response.getJSONArray("Result");
                            //Log.d("json obj", response.toString());

                            tehsilsList.clear();
                            Tehsil tZero = new Tehsil();
                            tZero.setId("0");
                            tZero.setName("--تحصیل کاانتخاب کریں--");
                            tehsilsList.add(tZero);
                            tehsilNamesList.add(tZero.getName());

                            for (int i = 0; i < dataArray.length(); i++) {
                                try {
                                    Tehsil tehsil = new Tehsil();
                                    JSONObject obj = dataArray.getJSONObject(i);
                                    // Pulling items from the array
                                    tehsil.setId(obj.getString("Id"));
                                    tehsil.setName(obj.getString("TehsilName"));
                                    tehsilsList.add(tehsil);
                                    tehsilNamesList.add(obj.optString("TehsilName"));
                                } catch (JSONException e) {
                                    Log.d("Error: ", e.getMessage());
                                }
                            }
                            //tehsilAdapter.setDropDownViewResource(android.R.layout.select_dialog_singlechoice);
                            //tehsilSpinner.setAdapter(tehsilAdapter);
                            customTehsilSpinnerAdapter = new MutationByTokenActivity.CustomSpinnerAdapter(MutationByTokenActivity.this, tehsilNamesList);
                            tehsilSpinner.setAdapter(customTehsilSpinnerAdapter);
                        } catch (JSONException e) {
                            Log.d("Error: ", e.getMessage());
                        }
                        progressDialog.hide();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        //add request to queue
        requestQueue.add(jsonObjectRequest);
    }


    public class CustomSpinnerAdapter extends BaseAdapter implements SpinnerAdapter {

        private final Context activity;
        private ArrayList<String> asr;

        public CustomSpinnerAdapter(Context context, ArrayList<String> asr) {
            this.asr = asr;
            activity = context;
        }

        public int getCount() {
            return asr.size();
        }

        public Object getItem(int i) {
            return asr.get(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            TextView txt = new TextView(MutationByTokenActivity.this);
            txt.setPadding(16, 16, 25, 16);
            txt.setTextSize(20);
            txt.setTypeface(appTextTypeFace, Typeface.BOLD);

            //txt.setGravity(Gravity.CENTER_VERTICAL);
            txt.setText(asr.get(position));
            txt.setTextDirection(View.TEXT_DIRECTION_RTL);
            txt.setTextColor(Color.parseColor("#70000000"));
            //txt.setCheckMarkDrawable(android.R.drawable.btn_radio);
            return txt;
        }

        public View getView(int i, View view, ViewGroup viewgroup) {
            TextView txt = new TextView(MutationByTokenActivity.this);
            //txt.setGravity(Gravity.RIGHT);
            txt.setPadding(16, 16, 25, 16);
            txt.setTextSize(20);
            txt.setTypeface(appTextTypeFace, Typeface.BOLD);
            txt.setText(asr.get(i));
            txt.setTextColor(Color.parseColor("#70000000"));
            txt.setTextDirection(View.TEXT_DIRECTION_RTL);
            return txt;
        }

    }
}
