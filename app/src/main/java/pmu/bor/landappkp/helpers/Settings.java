package pmu.bor.landappkp.helpers;

/**
 * Created by Rashid Ahmad on 3/7/2018.
 */

public class Settings {
    private final String URDU_FONT = "Jameel_Noori_Nastaleeq.ttf";
    private final int NIC_LENGTH = 13;

    public String getUrduFontPath() {
        return this.URDU_FONT;
    }

    public int getNICLength() {
        return  NIC_LENGTH;
    }
}
