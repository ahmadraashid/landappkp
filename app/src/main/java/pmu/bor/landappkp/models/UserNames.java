package pmu.bor.landappkp.models;

/**
 * Created by Rashid Ahmad on 2/28/2018.
 */

public class UserNames {
    private String personId;
    private String name;

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public String getPersonId() {
        return this.personId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}
