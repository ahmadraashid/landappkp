package pmu.bor.landappkp.helpers;

/**
 * Created by Rashid Ahmad on 3/8/2018.
 */

public class EngToUrduCharMap {
    char ch;

    public String getUrduString(String input) {
        String output = "";
        int textLength = input.length();

        if (textLength > 0) {
            char[] charArray = input.toCharArray();
            for(int v=0; v < charArray.length; v++) {
                char ch = mapCharacter(charArray[v]);
                output += ch;
            }
        }
        return output;
    }

    private char mapCharacter(char ch) {
        char mappedChar = ch;
        switch (ch) {
            case 'a':
                mappedChar = 'ا';
            break;

            case 'b':
                mappedChar = 'ب';
                break;

            case 'c':
                mappedChar = 'س';
                break;

            case 'd':
                mappedChar = 'د';
                break;

            case 'e':
                mappedChar = 'و';
                break;

            case 'f':
                mappedChar = 'ف';
                break;

            case 'g':
                mappedChar = 'گ';
                break;

            case 'h':
                mappedChar = 'ح';
                break;

            case 'i':
                mappedChar = 'ی';
                break;

            case 'j':
                mappedChar = 'ج';
                break;

            case 'k':
                mappedChar = 'ک';
                break;

            case 'l':
                mappedChar = 'ل';
                break;

            case 'm':
                mappedChar = 'م';
                break;

            case 'n':
                mappedChar = 'ن';
                break;

            case 'o':
                mappedChar = 'و';
                break;

            case 'p':
                mappedChar = 'پ';
                break;

            case 'q':
                mappedChar = 'ق';
                break;

            case 'r':
                mappedChar = 'ر';
                break;

            case 's':
                mappedChar = 'م';
                break;

            case 't':
                mappedChar = 'ت';
                break;

            case 'u':
                mappedChar = 'ے';
                break;

            case 'w':
                mappedChar = 'و';
                break;

            case 'x':
                mappedChar = 'ص';
                break;

            case 'y':
                mappedChar = 'ی';
                break;

            case 'z':
                mappedChar = 'ذ';
                break;

        }
        return mappedChar;
    }
}
