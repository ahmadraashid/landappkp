package pmu.bor.landappkp.models;

/**
 * Created by Rashid Ahmad on 1/29/2018.
 */

public class District {
    private String id;
    private String name;

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }
}
